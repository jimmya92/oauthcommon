public struct Scope: Codable {
    
    public let identifier: String
    public let description: String
    
    public init(identifier: String, description: String) {
        self.identifier = identifier
        self.description = description
    }
    
    public var main: String {
        let parts = identifier.split(separator: ":")
        if let main = parts.first {
            return String(main)
        }
        return identifier
    }
    
    public var sub: String? {
        let parts = identifier.split(separator: ":")
        guard parts.count == 2, let sub = parts.last else { return nil }
        return String(sub)
    }
}

public extension Scope {
    
    static var all: Scope {
        return .init(identifier: "*", description: "Access to everything")
    }
}

extension Scope: Equatable {
    
    public static func ==(lhs: Scope, rhs: Scope) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}
