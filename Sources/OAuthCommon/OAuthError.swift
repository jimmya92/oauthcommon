public enum OAuthError: Error {
    
    case invalidClient
    case invalidScopes
    case encoding
}
