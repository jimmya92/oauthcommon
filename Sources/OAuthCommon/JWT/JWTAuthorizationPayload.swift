import JWT

public struct JWTAuthorizationPayload: JWTPayload {
    
    public let exp: ExpirationClaim
    public let sub: SubjectClaim?
    public let iss: IssuerClaim
    public let scopes: [String]?
    
    public init(exp: ExpirationClaim, sub: SubjectClaim?, iss: IssuerClaim, scopes: [String]?) {
        self.exp = exp
        self.sub = sub
        self.iss = iss
        self.scopes = scopes
    }
    
    public func verify(using signer: JWTSigner) throws {
        try exp.verifyNotExpired()
    }
}
