public struct ClientConfig {
    
    public let id: String
    public let secret: String
    public let scopes: [Scope]
    
    public init(id: String, secret: String, scopes: [Scope]) {
        self.id = id
        self.secret = secret
        self.scopes = scopes
    }
    
    public func validateScopes(_ scopes: [Scope]) -> Bool {
        return scopes.allSatisfy { self.scopes.contains($0) }
    }
}
