import XCTest

import OAuthCommonTests

var tests = [XCTestCaseEntry]()
tests += OAuthCommonTests.allTests()
XCTMain(tests)
